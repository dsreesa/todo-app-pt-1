import React, { Component } from "react";
import todosList from "./todos.json";
import { v4 as uuidv4 } from 'uuid';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import TodoItem from './components/TodoItem/TodoItem'
import TodoList from './components/TodoList'

  // Source:Kano Marvel, Jordan Kubista , Joshua Michel 

  class App extends Component {
    state = {
      todos: todosList,
      value:""
    };
    ACTION ={
      ADD_TODO : 'add-todo',
      TOGGLE_TODO: 'toggle-todo',
      DELETE_TODO : 'delete todo'
    }

    handleSubmit = (event) => {
      if (event.key === "Enter") {
        // console.log("yeah")
        this.handleAddTodo()
      }
    }
    handleAddTodo = () => {
      const newTodo ={
        "userId" :1,
        "id": uuidv4(),
        "title" : this.state.value,
        "completed": false
      }
    const newTodos = [...this.state.todos]
        this.setState({
          todos:newTodos,
          value:""
        })
    
    }
    handleDelete = (todoId) => {
      const newTodos = this.state.todos.filter(
        todoItem => todoItem.id !== todoId
      )
      this.setState({ todos: newTodos })
    }
    handleCheck =(checkId)=>{
      const newTodos = this.state.todos.map(
        todoItem => {
          if (todoItem.id === checkId){
            todoItem.completed = !todoItem.completed
          }
          return todoItem
        }
      )
      this.setState({ todos: newTodos })
    }
  handleDeleteDone = () =>{
    const newTodos = this.state.todos.filter(
      todoItem => todoItem.completed === false
    )
    this.setState({ todos: newTodos })
  }
  
    newTodo = 
      {
        "userId": 1,
        "id": 1,
        "title": "delectus aut autem",
        "completed": false
      }
  
    handleNewText =(event)=>{
      let freshId 
      let freshTodoItem
      if (event.keyCode===13){
        freshId=this.state.todos.length+1
        let content = document.getElementById("addNewTextToDo").value
        freshTodoItem = {completed:false, id:freshId, title:content, userId:1}
        this.setState({todos: this.state.todos.concat(freshTodoItem)
        })
      }
      
      this.setState({value: event.target.value})
    }
   handleReducer = (todos, action) =>{
     switch(action.type){
       case this.ACTION.ADD_TODO:
         return [...todos, this.newTodo(action.payload.name)]
         case this.ACTION.TOGGLE_TODO:
           return todos.map(todo => {
             if (todo.id === action.payload.id){
               return { ...todo,complete: !todo.complete}
             }
             return todo
           })
           case this.ACTION.DELETE_TODO:
             return todos.filter(todo => todo.id !== action.payload.id)
             default:
               return todos
     }
   }

  render() {
    document.addEventListener("keydown",this.handleNewText)
    return (
      <Router>
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input 
          type= "text"
          onChange= {this.handleNewText}
          onKeyDown ={this.handleSubmit}
          value= {this.state.value ? this.state.value : ' '}
          id = "addNewTextToDo" 
          className="new-todo" 
          placeholder="What needs to be done?" 
          autoFocus />
        </header>

          <Route exact path ="/">
        <TodoList 
        todos={this.state.todos} 
        handleCheck = {this.handleCheck}
        handleDelete={this.handleDelete}/>
        </Route>

        <Route path = "/active">
          
            <TodoList
            todos={this.state.todos.filter(todo => todo.completed === false)} 
            handleCheck = {this.handleCheck}
            handleDelete={this.handleDelete}/>
          
        </Route>

        <Route 
        path = "/Completed">
          <TodoList
            todos={this.state.todos.filter(todo => todo.completed ===true)} 
            handleCheck = {this.handleCheck}
            handleDelete={this.handleDelete}/>
        
        </Route>

        <footer className="footer">
          <span className="todo-count">
            <strong>
              {this.state.todos.filter(todo => todo.complete === !true).length}
              </strong>{''}
               item(s) left
          </span>
          <ul className="filters">
    <li>
      <a href="/">All</a>
    </li>
    <li>
      <a href="/active">Active</a>
    </li>
    <li>
      <a href="/completed">Completed</a>
    </li>
  </ul>
          <button  
          onClick = {this.handleDeleteDone} 
          className="clear-completed">Clear completed</button>
          
        </footer>
      </section>
  </Router>

    );
  }
}

export default App
